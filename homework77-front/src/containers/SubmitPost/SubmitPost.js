import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createPost, onChange, onChangeFileRedux} from "../../store/actions";
import {Button, Grid, TextField} from "@material-ui/core";
import FileInput from "../../components/UI/FileInput/FileInput";

const SubmitPost = () => {

    const [post, setPost] = useState({
        image: '',
        message: '',
        author: '',
    })

    const dispatch = useDispatch();

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(post).forEach(key => {
            formData.append(key, post[key]);
        });
        dispatch(createPost(formData));
    }

    const onChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setPost(prevState => ({
            ...prevState,
            [name]: value
        }));
        // dispatch(onChange(e.target));

    };
    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setPost(prevState => ({
            ...prevState,
            [name]: file
        }));
        // dispatch(onChangeFileRedux(e.target));
    };

    return (
        <form onSubmit={submitFormHandler}>
            <Grid container direction={"column"} spacing={2}>
                <Grid item>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id='author'
                        label='Author'
                        name='author'
                        value={post.author}
                        onChange={onChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <TextField
                        fullWidth
                        variant="outlined"
                        id='message'
                        label='Message'
                        name='message'
                        value={post.message}
                        onChange={onChangeHandler}
                        required
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        name={'image'}
                        label={'Image'}
                        value={post.image}
                        changeHandler={onFileChange}
                    />
                </Grid>
            </Grid>
            <Button type={"submit"} variant="contained" color="primary">Send</Button>
        </form>
    );
};

export default SubmitPost;