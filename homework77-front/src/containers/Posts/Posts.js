import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@material-ui/core";
import PostItem from "../../components/PostItem/PostItem";
import {fetchPosts} from "../../store/actions";

const Posts = () => {


    const dispatch = useDispatch();
    const state = useSelector(state => state);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);


    return (
        <Grid container direction="column" mb={5}>
            {state.posts.map(post => {
                return (
                 <PostItem
                     key={post.id}
                     author={post.author}
                     image={post.image}
                     message={post.message}
                 />
                )
            })}
        </Grid>

    );
};

export default Posts;