import React from 'react';
import {Card, CardContent, CardMedia, Typography} from "@material-ui/core";
import makeStyles from "@material-ui/styles/makeStyles";

const useStyles = makeStyles({
    root: {
        minWidth: 200,
    },
    media: {
        height: 300,
    },
});

const PostItem = ({author, message, image}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root} >
                <CardMedia
                    className={classes.media}
                    image={'http://localhost:8000/uploads/' + image}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {author}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {message}
                    </Typography>
                </CardContent>
        </Card>
    );
}

export default PostItem;