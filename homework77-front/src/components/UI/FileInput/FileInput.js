import React, {useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import {onChangeFile} from "../../../store/actions";

const useStyles = makeStyles({
    // input: {
    //     display: 'none'
    // }
});

const FileInput = ({changeHandler, name, value, label}) => {
    const classes = useStyles();

    const inputRef = useRef();

    // const activateInput = () => {
    //     inputRef.current.click();
    // };

    const fileChangeTest = (e) => {
        changeHandler(e)
    }


    return (
        <>
            <input
                type={"file"}
                name={name}
                className={classes.input}
                onChange={fileChangeTest}
                ref={inputRef}
            />
            {/*<Grid container spacing={2} alignItems={"center"}>*/}
            {/*    <Grid item xs>*/}
            {/*        <TextField*/}
            {/*            variant={"outlined"}*/}
            {/*            disabled*/}
            {/*            fullWidth*/}
            {/*            label={label}*/}
            {/*            value={value}*/}
            {/*            onClick={activateInput}*/}
            {/*        />*/}
            {/*    </Grid>*/}
            {/*    <Grid item>*/}
            {/*        <Button*/}
            {/*            variant={"contained"}*/}
            {/*            onClick={activateInput}*/}
            {/*        >*/}
            {/*            Browse*/}
            {/*        </Button>*/}
            {/*    </Grid>*/}
            {/*</Grid>*/}

        </>
    );
};

export default FileInput;