import axiosApi from "../axiosApi";


export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';

export const ADD_POST = 'ADD_POST';
export const ON_CHANGE = 'ON_CHANGE';
export const ON_CHANGE_FILE = 'ON_CHANGE_FILE'
export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostsFailure = () => ({type: FETCH_POSTS_FAILURE});

export const addPost = data => ({type: ADD_POST, data});
export const onChange = (e) => ({type: ON_CHANGE, e});
export const onChangeFileRedux = payload => ({type: ON_CHANGE_FILE, payload});

export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/posts');
            dispatch(fetchPostsSuccess(response.data));
        } catch (e) {
            dispatch(fetchPostsFailure());
        }
    }
}

export const createPost = data => {
    return async dispatch => {
        try {
            // const obj = {
            //     author: data.author,
            //     message: data.message,
            //     image: data.image
            // }
            const response = await axiosApi.post('/posts', data);
            dispatch(addPost(response.data));
        } catch (e) {

        }
    }
}