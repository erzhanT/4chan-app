import {
    ADD_POST,
    FETCH_POSTS_FAILURE,
    FETCH_POSTS_REQUEST,
    FETCH_POSTS_SUCCESS,
    ON_CHANGE,
    ON_CHANGE_FILE
} from "./actions";

const initialState = {
    posts: [],
    author: '',
    message: '',
    image: '',
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return {...state};
        case  FETCH_POSTS_SUCCESS:
            return {...state, posts: action.posts};
        case FETCH_POSTS_FAILURE:
            return {...state};
        case ADD_POST:
            return {...state, data: action.data};
        case ON_CHANGE:
            return {...state, [action.e.name]: action.e.value};
        case ON_CHANGE_FILE:
            console.log(action.payload);
            return {...state, image: action.payload.files[0]};
        default:
            return state
    }
}

export default reducer;