import React from "react";
import './App.css';
import {Container, CssBaseline} from "@material-ui/core";
import Posts from "./containers/Posts/Posts";
import SubmitPost from "./containers/SubmitPost/SubmitPost";

const App = () => {


    return (
        <>
          <CssBaseline />
          <Container>
            <Posts />
            <SubmitPost />
          </Container>
        </>
    )
};

export default App;
