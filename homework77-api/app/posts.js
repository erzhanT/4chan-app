const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fileDb');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();


router.get('/', async (req, res) => {
    const posts = await fileDb.getItems();
    res.send(posts);
});

router.post('/', upload.single('image'), async (req, res) => {
    const post = req.body;
    if (post.author === '') {
        post.author = 'Anonymous'
    }
    if (req.file) {
        post.image = req.file.filename;
    }
    await fileDb.addItem(req.body);
    res.send(post);


});

module.exports = router;