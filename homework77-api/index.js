const express = require('express');
const cors = require("cors");
const fileDb = require('./fileDb');
const posts = require('./app/posts');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/posts', posts);

const port = 8000;


const run = async () => {
    await fileDb.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(console.error)



